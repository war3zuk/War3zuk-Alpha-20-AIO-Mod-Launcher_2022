Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,latam,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese,spanish

ZombieW7WalkStyle0Schematic,items,perk  book,Zombie Walking Style 0
ZombieW7WalkStyle0blend1Schematic,items,perk  book,Blended Zombie Walking Style
ZombieW7WalkStyle1Schematic,items,perk  book,Zombie Walking Style 1

zombieSanta,entityclasses,Entity,,,Mall Santa
zombieSantaFeral,entityclasses,Entity,,,Feral Mall Santa
zombieNurseTemplate,entityclasses,Entity,,,Infected Nurse
zombieNurseTemplateFeral,entityclasses,Entity,,,Feral Nurse
zombieFemaleTutorial,entityclasses,Entity,,,Infected Neighbor
zombieFemaleTutorialFeral,entityclasses,Entity,,,Feral Neighbor
zombieGuppyBlackSoldierMaskHelmet,entityclasses,Entity,,,Masked Soldier
zombieGuppyBlackSoldierMaskHelmetFeral,entityclasses,Entity,,,Feral Masked Soldier
zombieGuppyBlackSoldierHelmetNoMask,entityclasses,Entity,,,Infected Soldier
zombieGuppyBlackSoldierHelmetNoMaskFeral,entityclasses,Entity,,,Feral Solder
zombieGuppySoldier01,entityclasses,Entity,,,Soldier
zombieGuppySoldier01Feral,entityclasses,Entity,,,Feral Soldier
zombieGuppySoldier01Crawler,entityclasses,Entity,,,Crawler Soldier
zombieGuppySoldier01CrawlerFeral,entityclasses,Entity,,,Feral Crawler Soldier
zombieGuppySoldierFemale01,entityclasses,Entity,,,Lieutenant
zombieGuppySoldierFemale01Feral,entityclasses,Entity,,,Feral Lieutenant
zombieGuppySoldierFemale01Helmet,entityclasses,Entity,,,Cadet
zombieGuppySoldierFemale01HelmetFeral,entityclasses,Entity,,,Feral Cadet
zombieGuppySoldierLeader,entityclasses,Entity,,,Infected General
zombieGuppySoldierLeaderFeral,entityclasses,Entity,,,Feral General
zombieGuppyDoctor01,entityclasses,Entity,,,Infected Doctor
zombieGuppyDoctor01Feral,entityclasses,Entity,,,Feral Doctor
zombieGuppyDoctor02,entityclasses,Entity,,,Infected Surgeon
zombieGuppyDoctor02Feral,entityclasses,Entity,,,Feral Surgeon
zombieGuppyDoctor03,entityclasses,Entity,,,Infected Physician
zombieGuppyDoctor03Feral,entityclasses,Entity,,,Feral Physician
zombieGuppyDoctor04Black,entityclasses,Entity,,,Assistant Surgeon
zombieGuppyDoctor04BlackFeral,entityclasses,Entity,,,Feral Assistant Surgeon
zombieGuppyDoctorFemale01,entityclasses,Entity,,,Infected Doctor
zombieGuppyDoctorFemale01Feral,entityclasses,Entity,,,Feral Doctor
zombieGuppyDoctorFemale02,entityclasses,Entity,,,Infected Surgeon
zombieGuppyDoctorFemale02Feral,entityclasses,Entity,,,Feral Surgeon
zombieHazmatFemale01TSBX,entityclasses,Entity,,,The Quarantined
zombieHazmatFemale01TSBXFeral,entityclasses,Entity,,,Feral Quarantined
zombieGuppyMalePatient,entityclasses,Entity,,,Patient Zero
zombieGuppyProstitute,entityclasses,Entity,,,Stripper
zombieGuppyAlma,entityclasses,Entity,,,Infected Child
zombieRekt,entityclasses,Entity,,,Infected Farmer
zombieRektFeral,entityclasses,Entity,,,Feral Farmer
zombieRektRadiated,entityclasses,Entity,,,Radiated Farmer
zombieWhiteClown,entityclasses,Entity,,,Crazed
zombieHugh,entityclasses,Entity,,,Frozen Hugh
zombieHughFeral,entityclasses,Entity,,,Feral Frozen Hugh
zombieHughRadiated,entityclasses,Entity,,,Radiated Frozen Hugh
zombieGuppyBaldCop,entityclasses,Entity,,,Infected Cop
zombieGuppyBaldMan,entityclasses,Entity,,,Infected
zombieGuppyBeatCop,entityclasses,Entity,,,Infected Deputy
zombieGuppyBelle,entityclasses,Entity,,,Infected Schoolgirl
zombieGuppyCarmela,entityclasses,Entity,,,The Walker
zombieGuppyLucy,entityclasses,Entity,,,Hungry Child
zombieMalacayFCiv01,entityclasses,Entity,,,Infected Civilian
zombieGuppyPete,entityclasses,Entity,,,Pete
zombieGirl,entityclasses,Entity,,,Infected Teacher
zombieGuppyHungryJeff,entityclasses,Entity,,,Hungry Jeff
zombieGuppyNurse,entityclasses,Entity,,,Infected Nurse's Aid
zombieGuppyOldManZombie,entityclasses,Entity,,,Infected Hobo
zombieGuppyPoliceRalph,entityclasses,Entity,,,Officer Ralph
zombieGuppySeth,entityclasses,Entity,,,The Chosen One
zombieCopSheriffTSBX,entityclasses,Entity,,,Infected Sheriff
zombieCopSheriffTSBXFeral,entityclasses,Entity,,,Feral Sheriff
zombieCopSwat01TSBX,entityclasses,Entity,,,Infected Swat Member
zombieCopSwat01TSBXFeral,entityclasses,Entity,,,Feral Swat Member
zombieCopCityFemale01TSBX,entityclasses,Entity,,,Infected Officer
zombieCopCityFemale01TSBXFeral,entityclasses,Entity,,,Feral Officer
zombieHoly01MUMPFY,entityclasses,Entity,,,Lost Raver
zombieHoly01MUMPFYFeral,entityclasses,Entity,,,Feral Raver
zombieGuppyPest,entityclasses,Entity,,,Pest
zombieGuppyBiomechanicalWight,entityclasses,Entity,,,Burnt Wight
zombieGuppyClot,entityclasses,Entity,,,Clot
zombieGuppyCreepyCrawly,entityclasses,Entity,,,Creepy Crawly
zombieGuppyInfernalDog,entityclasses,Entity,,,Infernal Dog
zombieBehemoth,entityclasses,Entity,,,Behemoth
zombieGuppyAbonimation,entityclasses,Entity,,,The Abomination

junkTurretSledge,entityclasses,Entity,,,Sledge Turret
junkTurretGun,entityclasses,Entity,,,Turret
animalMountainLion,entityclasses,Entity,,,Mountain Lion
animalZombieDog,entityclasses,Entity,,,Infected Dog
animalCoyote,entityclasses,Entity,,,Coyote
animalWolf,entityclasses,Entity,,,Wolf
animalDireWolf,entityclasses,Entity,,,Dire Wolf
animalSnake,entityclasses,Entity,,,Snake
animalBear,entityclasses,Entity,,,Wild Bear
animalZombieBear,entityclasses,Entity,,,Infected Bear
animalBoar,entityclasses,Entity,,,Wild Boar
animalBossGrace,entityclasses,Entity,,,Radiated Boar
animalZombieVulture,entityclasses,Entity,,,Infected Vulture
animalZombieVultureRadiated,entityclasses,Entity,,,Radiated Vulture
zombieJanitor,entityclasses,Entity,,,Infected Janitor
zombieJanitorFeral,entityclasses,Entity,,,Feral Janitor
zombieJanitorRadiated,entityclasses,Entity,,,Radiated Janitor
zombieArleneRadiated,entityclasses,Entity,,,Radiated Putrid Girl
zombieBiker,entityclasses,Entity,,,Infected Biker
zombieBikerFeral,entityclasses,Entity,,,Feral Biker
zombieBikerRadiated,entityclasses,Entity,,,Radiated Biker
zombieSkateboarder,entityclasses,Entity,,,Infected Skater Punk
zombieSkateboarderFeral,entityclasses,Entity,,,Feral Skater
zombieSkateboarderRadiated,entityclasses,Entity,,,Radiated Punk
zombieBoeRadiated,entityclasses,Entity,,,Radiated Infected Survivor
zombieCheerleader,entityclasses,Entity,,,Infected Cheerleader
zombieCheerleaderFeral,entityclasses,Entity,,,Feral Cheerleader
zombieCheerleaderRadiated,entityclasses,Entity,,,Radiated Cheerleader
zombieDarleneRadiated,entityclasses,Entity,,,Radiated Decayed Mother
zombieFemaleFatRadiated,entityclasses,Entity,,,Radiated Hungry Zombie
zombieJoeRadiated,entityclasses,Entity,,,Radiated Cadaver
zombieMarleneRadiated,entityclasses,Entity,,,Radiated Woman
zombieMoeRadiated,entityclasses,Entity,,,Radiated Bloated Walker
zombieOldTimerRadiated,entityclasses,Entity,,,Radiated Zombie Cowboy
zombieScreamerRadiated,entityclasses,Entity,,,Radiated Screamer
zombieSoldierRadiated,entityclasses,Entity,,,Radiated Soldier
zombieSpiderRadiated,entityclasses,Entity,,,Radiated Spider Zombie
zombieUtilityWorker,entityclasses,Entity,,,Infected Utility Worker
zombieUtilityWorkerFeral,entityclasses,Entity,,,Feral Utility Worker
zombieYoRadiated,entityclasses,Entity,,,Radiated Carcass
zombieDemolition,entityclasses,Entity,,,The Demolition Man
zombieWightRadiated,entityclasses,Entity,,,Radiated Wight
zombieLab,entityclasses,Entity,,,Infected Lab Assistant
zombieLabFeral,entityclasses,Entity,,,Feral Lab Assistant
zombieLabRadiated,entityclasses,Entity,,,Radiated Lab Assistant
legendaryZombieLab,entityclasses,Entity,,,Legendary Lab Assistant
legendaryZombieBusinessMan,entityclasses,Entity,,,Legendary Business Man
legendaryZombieUtilityWorker,entityclasses,Entity,,,Legendary Utility Worker